//////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author AJ Patalinghog <ajpata@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   11 March 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "animal.hpp"
#include "animalFactory.hpp"
#include "list.hpp"

using namespace std;
using namespace animalfarm;

int main() {
   cout << "Welcome to Animal Farm 4" << endl;

   SingleLinkedList animalList; 
   for( auto i = 0 ; i < 25 ; i++ ) {
      animalList.push_front( AnimalFactory::getRandomAnimal() ) ;
   }

   Node* a = animalList.get_first();

   cout << endl;
   cout << "List of Animals" << endl;
   cout << "  Is it empty: " << boolalpha << animalList.empty() << endl;
   cout << "  Number of elements: " << animalList.size() << endl;

   for( auto animal = animalList.get_first() ; animal != nullptr ; animal = animalList.get_next( animal )) {
   cout << ((Animal*)animal)->speak() << endl;
   }

   while( !animalList.empty() ) {
      Animal* animal = (Animal*) animalList.pop_front();
      delete animal;

   }
}
