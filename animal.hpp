///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author AJ Patalinghog <ajpata@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   11 March 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include "node.hpp" 

using namespace std;

namespace animalfarm {

int getRandInt(int min, int max);

enum Gender { MALE, FEMALE, UNKNOWN };

enum Color { BLACK, WHITE, RED, SILVER, YELLOW, BROWN}; 

class Animal : public Node {
public:
	enum Gender gender;
	string      species;

	Animal();
   ~Animal();

   virtual const string speak() = 0;
	
	void printInfo();
	
	string colorName  (enum Color color);
	string genderName (enum Gender gender);
   string trueOrFalse (bool isTrue);

   static const Gender getRandomGender();
   static const Color  getRandomColor();
   static const bool   getRandomBool();
   static const float  getRandomWeight( const float from, const float to );
   static const string getRandomName();
};

} // namespace animalfarm
