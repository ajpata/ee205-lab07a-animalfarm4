///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animalFactory.hpp
/// @version 1.0
///
/// Exports data about animal factory
///
/// @author AJ Patalinghog <ajpata@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   11 March 2021
///////////////////////////////////////////////////////////////////////////////
#include "animal.hpp"

using namespace std;

namespace animalfarm {
   
class AnimalFactory {
public:
   static Animal* getRandomAnimal();
};

}
