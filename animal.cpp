///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author AJ Patalinghog <ajpata@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   11 March 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <random>

#include "animal.hpp"

using namespace std;

namespace animalfarm {

int getRandInt(int min, int max) {
   random_device rd;
   mt19937_64 gen(rd());
   uniform_int_distribution<> randFrom(min,max);

   return randFrom(gen);
}

Animal::Animal() {
   cout << ".";
}

Animal::~Animal() {
   cout << "x";
}

void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};
	

string Animal::colorName (enum Color color) {
   switch (color) {
      case BLACK:    return string("Black"); break;
      case WHITE:    return string("White"); break;
      case RED:      return string("Red"); break;
      case SILVER:   return string("Silver"); break;
      case YELLOW:   return string("Yellow"); break;
      case BROWN:    return string("Brown"); break;
   }

   return string("Unknown");
};

string Animal::trueOrFalse (bool isTrue) {
   switch(isTrue) {
      case true:     return string("true"); break;
      case false:    return string("false"); break;
   }

   return string("Unknown");
}

const string Animal::getRandomName() {
   string name;
   int length = getRandInt(4,10);
   const string letters[2] = { "bcdfghjklmnpqrstvwxyz", "aeiouy" };

   for(int i = 0; i < length; i++) {
      int letterLength = letters[i%2].length();                 //length of the consonant or vowel string
      name += letters[i%2].substr(getRandInt(0,letterLength-1),1); //adds a random consonant or vowel to name
   }

   name[0] = toupper(name[0]); //capitalizes first letter

   return name;
}

const Gender Animal::getRandomGender() {
   return static_cast<Gender>(getRandInt(0,1)); // static casting a random int into corresponding enum Gender value
}

const Color Animal::getRandomColor() {
   return static_cast<Color>(getRandInt(0,5)); //static casting a random int into corresponding enum Color value
}

const bool Animal::getRandomBool() {
   random_device rd;
   mt19937_64 gen(rd());
   bernoulli_distribution randBool(0.5);

   return randBool(gen);
}

const float Animal::getRandomWeight(const float from, const float to) { 
   random_device rd;
   mt19937_64 gen(rd());
   uniform_real_distribution<float> randFloat(from, to);

   return randFloat(gen); 
}



} // namespace animalfarm
