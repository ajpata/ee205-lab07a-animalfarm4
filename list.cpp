///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.cpp
/// @version 1.0
///
/// @author AJ Patalinghog <ajpata@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   25 March 2021
///////////////////////////////////////////////////////////////////////////////

#include "list.hpp"
#include <iostream>
using namespace std;

namespace animalfarm {

const bool SingleLinkedList::empty() const{
   return head == nullptr;
}

void SingleLinkedList::push_front( Node* newNode ) {
   newNode->next = head;
   head = newNode;
}

Node* SingleLinkedList::pop_front() {
   if( empty() ){
      return nullptr;
   } else {
      Node* oldHead = head;
      head = head->next;
      return oldHead;
   }

}

Node* SingleLinkedList::get_first() const {
   return head;
}

Node* SingleLinkedList::get_next( const Node* currentNode ) const {
   return currentNode->next;
}

unsigned int SingleLinkedList::size() const {
   Node* current = head;
   unsigned int size = 1;
   
   if( empty() ) {
      return 0;
   } else {
      while(current->next != nullptr) {
         size++;
         current = get_next(current);
      }
   return size;
   }
}

}
