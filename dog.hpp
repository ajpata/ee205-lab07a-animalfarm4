///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file dog.hpp
/// @version 1.0
///
/// Exports data about all dogs
///
/// @author AJ Patalinghog <ajpata@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   11 March 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "mammal.hpp"

using namespace std;


namespace animalfarm {

class Dog : public Mammal {
public:
   string name;

   Dog( string newName, enum Color newColor, enum Gender newGender );

   virtual const string speak();

   void printInfo();
};

} // namespace animalfarm
