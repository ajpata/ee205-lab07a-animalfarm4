///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file mammal.hpp
/// @version 1.0
///
/// Exports data about all mammals
///
/// @author AJ Patalinghog <ajpata@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   11 March 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "animal.hpp"

namespace animalfarm {

class Mammal : public Animal {
public:
	enum Color hairColor;
	int        gestationPeriod;
	
	void printInfo();
};

} // namespace animalfarm
