///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.hpp
/// @version 1.0
///
/// @author AJ Patalinghog <ajpata@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   25 March 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "node.hpp"

using namespace std;

namespace animalfarm {

class SingleLinkedList {
protected:
   Node* head = nullptr;

public:
   const bool empty() const;        //Return true if the list is empty. false if there’s anything in the list.
   void push_front( Node* newNode );         //Add newNode to the front of the list.
   Node* pop_front();         //Remove a node from the front of the list. If the list is already empty, return nullptr.
   Node* get_first() const;         //Return the very first node from the list. Don’t make any changes to the list.
   Node* get_next( const Node* currentNode ) const;         //Return the node immediately following currentNode.
   unsigned int size() const;       //Return the number of nodes in the list
};

}
